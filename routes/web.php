<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return "hello world";
});

Route::get('/student/{id?}', function ($id ='no student provided') {
    return "hello student ".$id ." :)";
})->name('students');

Route::get('/comment/{id}', function ($id) {
    return view('comment',['id'=>$id]);
})->name('comments');


Route::get('/customers/{num?}', function ($num = 'No costumer was Provided') {
    if($num=='No costumer was Provided')
    return view('nocustomers', ['num'=>$num]);
    else
    return view('customers',['num'=>$num]);
    ;
})->name('customers');

Route::resource('todos', 'TodoController')->middleware('auth');;
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
